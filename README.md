## Description :

Un virement est un transfert d'argent d'un compte emetteur vers un compte bénéficiaire ...

**Besoin métier :** 

Ajouter un nouveau usecase versement. Le Versement est un dépôt d'agent sur un compte donné . 

Le versement est une opération trés utile lors des transfert de cash .
 
Imaginez que vous allez à une agence avec un montant de 1000DH et que vous transferez ça en spécifiant le RIB souhaité .
 
L'identifiant fonctionnel d'un compte dans ce cas préçis est le RIB .  


## Assignement :

* Le code présente des anomalies de qualité (bonnes pratiques , abstraction , lisibilité ...) et des bugs . 
    * localiser le maximum 
    * Essayer d'améliorer la qualité du code .    
    * Essayer de résoudre les bugs détectés. 
* Implementer le use case `Versement` 
* Ajouter des tests unitaires.  
* **Nice to have** : Ajouter une couche de sécurité 

## How to use 
To build the projet you will need : 
* Java 11+ 
* Maven

Build command : 
```
mvn clean install
```

Run command : 
```
./mvnw spring-boot:run 
## or use any prefered method (IDE , java -jar , docker .....)
```

## How to submit 
* Fork the project into your personal gitlab space .    
* Do the stuff .
* Send us the link .

## Bugs résolus :

_ (cascade = CascadeType.PERSIST) pour faciliter la persistence des entitées liées

_ Handler les exceptions au niveau de ersementController et VirementController

_ Remplacer  if (motif.length() < 0) par if (motif.length() == 0) au niveau de la verification 
de la longueur du motif

_ if(montantVirement.equals(null)) ==> on ne peut pas appeller equals sur un objet null 
la remplacer par if(montantVirement==null)

_ Ajouter @Transactional aux services et aux méthodes dans le controlleur pour s'assurer que
les operations liés persistent toutes ensemble sinon on rollback

_ Utiliser @DataJpaTest à la place de @SpringBootTest pour tester les repositories 

_ Utiliser TestEntityManager pour tester puis suprrimer les modifications apportées par 
les données des tests

_ Utiliser spring.jpa.hibernate.ddl-auto=create-drop pour les tests

## Amélioration apportées :
**Abstraction** 

_ Les deux Beans AuditVirement et AuditVersement héritent du bean abstrait Audit

_ Les deux classes VirementsServie et VersementsServie héritent de la classe abstraite Service
qui contient les méthodes communs entre virement et versement

**Booster la consistence de l'architecture de l'appliacation** 

_ Chaque service implémente une interface contenant les signatures des méthodes et 
les exceptions succeptibles d'être levées

**Réutilisabilité**

_ Diviser les méthodes compliquées en sous méthodes spécifiques à une tache précise pour 
les réutiliser après selon le besoin et faciliter les tests unitaires

**Test coverage**

_ Créer les tests unitaires coverage 100% pour les repositories et les services 
qui sont validés


_ J'ai pas pu complété les tests pour les controleurs vu la contraint du temps 

