package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class VirementServiceTest {

    @Autowired
    @Qualifier("VirementService")
    private VirementService virementService;

    @MockBean
    private VirementRepository virementRepository;

    @MockBean
    private CompteRepository compteRepository;

    long millis=System.currentTimeMillis();
    VirementDto virementDto=new VirementDto("010000B025001000", "010000A000001000"
            , "Don", new BigDecimal(130.00), new Date(millis));

    List<Virement>  virements=new ArrayList<Virement>();

    Compte compte0 = new Compte();

    @BeforeEach
    void setUp() {
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(200000L));
        compte1.setUtilisateur(utilisateur1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);

        Virement virement = new Virement();
        virement.setMontantVirement(BigDecimal.TEN);
        virement.setCompteBeneficiaire(compte2);
        virement.setCompteEmetteur(compte1);
        virement.setDateExecution(new Date());
        virement.setMotifVirement("Assignment 2021");

        virements.add(virement);
        if(virements.isEmpty())
            virements=null;

        compte0.setNrCompte("010000C000001000");
        compte0.setRib("RIB1");
        compte0.setSolde(BigDecimal.valueOf(99l));
        compte0.setUtilisateur(utilisateur1);

        when(virementRepository.findAll()).thenReturn(virements);

        when(compteRepository.findByNrCompte(compte0.getNrCompte())).thenReturn(compte0);

        when(compteRepository.findByNrCompte(compte1.getNrCompte())).thenReturn(compte1);

        when(compteRepository.findByNrCompte(compte2.getNrCompte())).thenReturn(compte2);

    }



    @Test
    void isSoldSufficient() {
        SoldeDisponibleInsuffisantException thrown = assertThrows(
                SoldeDisponibleInsuffisantException.class,
                () -> virementService.isSoldSufficient(compte0.getNrCompte(), new BigDecimal(100))
        );
        assertTrue(thrown.getMessage().contains("Solde insuffisant pour l'utilisateur"));
    }

    @Test
    void loadAllVirement() {
        List<Virement> returnedList= virementService.loadAllVirement();

        List<Virement> myList= virements;

        assertEquals(myList, returnedList);
    }

    @Test
    void isMontantVirementValid() {
        TransactionException thrown = assertThrows(
                TransactionException.class,
                () -> virementService.isMontantVirementValid(new BigDecimal(0) )
        );
        assertTrue(thrown.getMessage().contains("Montant vide"));

        TransactionException thrown1 = assertThrows(
                TransactionException.class,
                () -> virementService.isMontantVirementValid(null)
        );
        assertTrue(thrown1.getMessage().contains("Montant vide"));

        TransactionException thrown2 = assertThrows(
                TransactionException.class,
                () -> virementService.isMontantVirementValid(new BigDecimal(9))
        );
        assertTrue(thrown2.getMessage().contains("Montant minimal de virement non atteint"));

        TransactionException thrown3 = assertThrows(
                TransactionException.class,
                () -> virementService.isMontantVirementValid(new BigDecimal(10001))
        );
        assertTrue(thrown3.getMessage().contains("Montant maximal de virement dépassé"));
    }

    @Test
    void mapToVirement() {
        //create Versement based on versementDto
        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());

        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());
        virement.setCompteBeneficiaire(compteBeneficiaire);

        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        virement.setCompteEmetteur(compteEmetteur);

        virement.setMotifVirement(virementDto.getMotif());
        virement.setMontantVirement(virementDto.getMontantVirement());

        //create Versement based on mapToVersement(versementDto)
        Virement myVirement=virementService.mapToVirement(virementDto);

        //verify if they are equal
        assertEquals(virement, myVirement);
    }

    @Test
    void createVirement() {
        Virement virement=virementService.mapToVirement(virementDto);

        virementService.createVirement(virementDto);

        //needed to override equals & hashcode for both Account and Virement
        verify(virementRepository).save(virement);
    }
}