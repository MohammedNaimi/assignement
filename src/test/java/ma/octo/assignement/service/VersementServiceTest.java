package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.AccountType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.verify;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
class VersementServiceTest {
    @Autowired
    @Qualifier("VersementService")
    private VersementService versementService;

    @MockBean
    private VersementRepository versementRepository;

    @MockBean
    private CompteRepository compteRepository;

    @MockBean
    private UtilisateurRepository utilisateurRepository;

    long millis=System.currentTimeMillis();
    VersementDto versementDto=new VersementDto(new BigDecimal(8000.00), new Date(millis),
            "Octo Rh department", "010000B025001000", "Salaire");

    List<Versement>  versements=new ArrayList<Versement>();

    List<Compte>  comptes=new ArrayList<Compte>();

    List<Utilisateur>  utilisateurs=new ArrayList<Utilisateur>();

    @BeforeEach
    void setUp() {
        Utilisateur utilisateur=new Utilisateur(2l, "user2", "Female", "last2"
                , "first2", null);

        Compte compteBenificaire=new Compte(4l, "010000B025001000", "RIB2", new BigDecimal(140000.00)
                , utilisateur);

        Versement versement=new Versement(new BigDecimal(8000.00), new Date(millis),
                "Octo Rh department", compteBenificaire, "Salaire");

        versements.add(versement);
        if(versements.isEmpty())
            versements=null;

        //comptes.add(compteBenificaire);
        if(comptes.isEmpty())
            comptes=null;

        //utilisateurs.add(utilisateur);
        if(utilisateurs.isEmpty())
            utilisateurs=null;

        when(compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire())).thenReturn(compteBenificaire);

        when(versementRepository.findAll()).thenReturn(versements);

        when(compteRepository.findAll()).thenReturn(comptes);

        when(utilisateurRepository.findAll()).thenReturn(utilisateurs);
    }
    @Test
    void updateSoldeAccount(){
        /**/BigDecimal solde=versementService.updateSoldeAccount(versementDto.getNrCompteBeneficiaire(),
                versementDto.getMontantVersement(), AccountType.RECEIVER);

        BigDecimal expectedSolde=new BigDecimal(140000+8000);

        assertEquals( expectedSolde.intValue(), solde.intValue());

        //lorsque nous arrivons a cette etape solde est déja ==> on ne peut pas executer les deux tests
        // l'un apres l'autre
        BigDecimal solde1=versementService.updateSoldeAccount(versementDto.getNrCompteBeneficiaire(),
                versementDto.getMontantVersement(), AccountType.EMITTER);

        BigDecimal expectedSolde1=new BigDecimal(148000-8000);

        assertEquals( expectedSolde1.intValue(), solde1.intValue());

    }

    @Test
    void loadAllUtilisateur(){
        List<Utilisateur>  returnedList= versementService.loadAllUtilisateur();

        List<Utilisateur> myList=utilisateurs;

        assertEquals(myList, returnedList);
    }

    @Test
    void loadAllCompte(){
        List<Compte>  returnedList= versementService.loadAllCompte();

        List<Compte> myList=comptes;

        assertEquals(myList, returnedList);
    }

    @Test
    void isMotifEmpty(){
        TransactionException thrown = assertThrows(
                TransactionException.class,
                () -> versementService.isMotifEmpty("")
        );
        assertTrue(thrown.getMessage().contains("Motif vide"));
    }

    @Test
    void doesAccountExist(){
        CompteNonExistantException thrown = assertThrows(
                CompteNonExistantException.class,
                () -> versementService.doesAccountExist("45")
        );
        assertTrue(thrown.getMessage().contains("Compte Non existant"));
    }

    @Test
    void loadAllVersement() {
        List<Versement>  returnedList=versementService.loadAllVersement();

        List<Versement> myList=versements;

        assertEquals(myList, returnedList);
    }

    @Test
    void mapToVersement() {
        //create Versement based on versementDto
        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDate());

        versement.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());

        Compte compteBeneficiaire = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());
        versement.setCompteBeneficiaire(compteBeneficiaire);


        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setMontantVersement(versementDto.getMontantVersement());

        //create Versement based on mapToVersement(versementDto)
        Versement myVersement=versementService.mapToVersement(versementDto);

        //verify if they are equal
        assertEquals(versement, myVersement);
    }

    @Test
    void createVersement() {
        Versement versement=versementService.mapToVersement(versementDto);

        versementService.createVersement(versementDto);

        //needed to override equals & hashcode for both Account and Versement
        verify(versementRepository).save(versement);
    }
}