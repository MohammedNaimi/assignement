package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditVersementRepository;
import ma.octo.assignement.repository.AuditVirementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

@SpringBootTest
class AuditServiceTest {
    @Autowired
    private AuditService auditService;

    @MockBean
    private AuditVirementRepository auditVirementRepository;

    @MockBean
    private AuditVersementRepository auditVersementRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void auditVirement() {
        Audit audit=new AuditVirement("Virement efféctué", EventType.VIREMENT);
        auditService.auditVirement(audit);
        verify(auditVirementRepository).save(audit);
    }

    @Test
    void auditVersement() {
        Audit audit=new AuditVersement("Versement efféctué", EventType.VERSEMENT);
        auditService.auditVersement(audit);
        verify(auditVersementRepository).save(audit);
    }
}