package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
class VersementRepositoryTest {

    @Autowired
    private VersementRepository versementRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    Versement versement;
    Utilisateur utilisateur;
    Compte compteBeneficiaire;
    public Versement createVersement(){
        Utilisateur utilisateur=new Utilisateur( "user2", "Female", "last2"
                , "first2", null);

        //ne fonctionne pas avec un contructeur qui contient id
        Compte compteBeneficiaire=new Compte( "010000B025001000", "RIB2",
                new BigDecimal(140000.00), utilisateur);

        long millis=System.currentTimeMillis();
        Versement versement=new Versement(new BigDecimal(8000.00), new Date(millis),
                "Octo Rh department", compteBeneficiaire, "Salaire");

        return versement;
    }

    @BeforeEach
    void setUp() {
        versement=createVersement();

        testEntityManager.persist(versement);
    }

    @Test
    public void findById(){
        Optional<Versement> returnedVersement=versementRepository.findById(versement.getId()) ;
        assertTrue(returnedVersement.equals(Optional.of(versement)));
    }

    @Test
    void findAll(){
        List<Versement> returnedList=versementRepository.findAll();

        List<Versement> myList=new ArrayList<Versement>();
        myList.add(versement);

        assertEquals(myList, returnedList);
    }

    @Test
    public void save() {
        Versement returnedVirement=versementRepository.save(versement);
        assertEquals(versement, returnedVirement);
    }

    @Test
    public void delete() {
        boolean beofreDelete=versementRepository.findById(versement.getId()).isPresent();

        versementRepository.delete(versement);

        boolean afterDelete=versementRepository.findById(versement.getId()).isPresent();
        assertTrue(beofreDelete);
        assertFalse(afterDelete);
    }


}