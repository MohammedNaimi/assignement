package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.util.EventType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
class AuditVirementRepositoryTest {
    @Autowired
    private AuditVirementRepository auditVirementRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    Audit auditVirement;

    public Audit createAuditVirement(){
        Audit auditVersement=new AuditVersement("Versement effectué", EventType.VERSEMENT);
        return auditVersement;
    }

    @BeforeEach
    void setUp() {
        auditVirement=createAuditVirement();
        testEntityManager.persist(auditVirement);
    }

    @Test
    public void findById(){
        Optional<Audit> returnedAuditVirement=auditVirementRepository
                .findById(auditVirement.getId()) ;
        assertTrue(returnedAuditVirement.equals(Optional.of(auditVirement)));
    }

    @Test
    public void delete() {
        boolean beforeDelete=auditVirementRepository.findById(auditVirement.getId()).isPresent();

        auditVirementRepository.delete(auditVirement);

        boolean afterDelete=auditVirementRepository.findById(auditVirement.getId()).isPresent();
        assertTrue(beforeDelete);
        assertFalse(afterDelete);
    }

    @Test
    public void save() {
        Audit returnedAuditVirement=auditVirementRepository.save(auditVirement);
        assertEquals(auditVirement, returnedAuditVirement);
    }

    @Test
    public void findAll() {
        List<Audit> returnedList=auditVirementRepository.findAll();

        List<Audit> myList=new ArrayList<Audit>();
        myList.add(auditVirement);

        assertEquals(myList, returnedList);
    }
}