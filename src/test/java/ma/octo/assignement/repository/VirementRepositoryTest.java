package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;

  @Autowired
  private TestEntityManager testEntityManager;

  Virement virement;

  public Virement createVirement(){
    Utilisateur utilisateur1 = new Utilisateur();
    utilisateur1.setUsername("user1");
    utilisateur1.setLastname("last1");
    utilisateur1.setFirstname("first1");
    utilisateur1.setGender("Male");

    Utilisateur utilisateur2 = new Utilisateur();
    utilisateur2.setUsername("user2");
    utilisateur2.setLastname("last2");
    utilisateur2.setFirstname("first2");
    utilisateur2.setGender("Female");

    Compte compte1 = new Compte();
    compte1.setNrCompte("010000A000001000");
    compte1.setRib("RIB1");
    compte1.setSolde(BigDecimal.valueOf(200000L));
    compte1.setUtilisateur(utilisateur1);

    Compte compte2 = new Compte();
    compte2.setNrCompte("010000B025001000");
    compte2.setRib("RIB2");
    compte2.setSolde(BigDecimal.valueOf(140000L));
    compte2.setUtilisateur(utilisateur2);

    Virement virement = new Virement();
    virement.setMontantVirement(BigDecimal.TEN);
    virement.setCompteBeneficiaire(compte2);
    virement.setCompteEmetteur(compte1);
    virement.setDateExecution(new Date());
    virement.setMotifVirement("Assignment 2021");
    return virement;
  }

  @BeforeEach
  void setUp() {
    virement=createVirement();
    testEntityManager.persist(virement);
  }


  @Test
  public void findAll() {
    List<Virement> returnedList=virementRepository.findAll();

    List<Virement> myList=new ArrayList<Virement>();
    myList.add(virement);

    assertEquals(myList, returnedList);
  }

  @Test
  public void save() {
    Virement returnedVirement=virementRepository.save(virement);
    assertEquals(virement, returnedVirement);
  }

  @Test
  public void findById(){
    Optional<Virement> returnedVirement=virementRepository.findById(virement.getId()) ;
    assertTrue(returnedVirement.equals(Optional.of(virement)));
  }

  @Test
  public void delete() {
    boolean beforeDelete=virementRepository.findById(virement.getId()).isPresent();

    virementRepository.delete(virement);

    boolean afterDelete=virementRepository.findById(virement.getId()).isPresent();
    assertTrue(beforeDelete);
    assertFalse(afterDelete);
  }
}