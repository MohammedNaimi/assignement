package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
class CompteRepositoryTest {
    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    Compte compte;

    public Compte createCompte(){
        Utilisateur utilisateur=new Utilisateur( "user2", "Female", "last2"
                , "first2", null);

        //ne fonctionne pas avec un contructeur qui contient id
        Compte compteBeneficiaire=new Compte( "010000B025001000", "RIB2",
                new BigDecimal(140000.00), utilisateur);
        return compteBeneficiaire;
    }
    @BeforeEach
    void setUp() {
        compte=createCompte();
        testEntityManager.persist(compte);
    }

    @Test
    public void findById(){
        Optional<Compte> returnedCompte=compteRepository.findById(compte.getId()) ;
        assertTrue(returnedCompte.equals(Optional.of(compte)));
    }

    @Test
    public void delete() {
        boolean beforeDelete=compteRepository.findById(compte.getId()).isPresent();

        compteRepository.delete(compte);

        boolean afterDelete=compteRepository.findById(compte.getId()).isPresent();
        assertTrue(beforeDelete);
        assertFalse(afterDelete);
    }

    @Test
    public void save() {
        Compte returnedCompte=compteRepository.save(compte);
        assertEquals(compte, returnedCompte);
    }

    @Test
    public void findAll() {
        List<Compte> returnedList=compteRepository.findAll();

        List<Compte> myList=new ArrayList<Compte>();
        myList.add(compte);

        assertEquals(myList, returnedList);
    }

    @Test
    void findByNrCompte() {
        Compte returnedCompte=compteRepository.findByNrCompte(compte.getNrCompte());

        Compte myCompte=compte;
        assertEquals(myCompte, returnedCompte);

    }
}