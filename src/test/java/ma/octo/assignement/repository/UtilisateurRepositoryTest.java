package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
class UtilisateurRepositoryTest {
    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    Utilisateur utilisateur;

    public Utilisateur createUtilisateur(){
        Utilisateur utilisateur=new Utilisateur( "user2", "Female", "last2"
                , "first2", null);

        return utilisateur;
    }

    @BeforeEach
    void setUp() {
        utilisateur=createUtilisateur();
        testEntityManager.persist(utilisateur);
    }

    @Test
    public void findById(){
        Optional<Utilisateur> returnedUtilisateur=utilisateurRepository.findById(utilisateur.getId()) ;
        assertTrue(returnedUtilisateur.equals(Optional.of(utilisateur)));
    }

    @Test
    public void delete() {
        boolean beforeDelete=utilisateurRepository.findById(utilisateur.getId()).isPresent();

        utilisateurRepository.delete(utilisateur);

        boolean afterDelete=utilisateurRepository.findById(utilisateur.getId()).isPresent();
        assertTrue(beforeDelete);
        assertFalse(afterDelete);
    }


    @Test
    public void save() {
        Utilisateur returnedUtilisateur=utilisateurRepository.save(utilisateur);
        assertEquals(utilisateur, utilisateur);
    }

    @Test
    public void findAll() {
        List<Utilisateur> returnedList=utilisateurRepository.findAll();

        List<Utilisateur> myList=new ArrayList<Utilisateur>();
        myList.add(utilisateur);

        assertEquals(myList, returnedList);
    }


}