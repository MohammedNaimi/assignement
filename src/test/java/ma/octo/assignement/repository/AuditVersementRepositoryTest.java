package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.domain.AuditVersement;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.util.EventType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Transactional
class AuditVersementRepositoryTest {

    @Autowired
    private AuditVersementRepository auditVersementRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    Audit auditVersement;

    public Audit createAuditVersement(){
        Audit auditVersement=new AuditVersement("Versement effectué", EventType.VERSEMENT);
        return auditVersement;
    }

    @BeforeEach
    void setUp() {
        auditVersement=createAuditVersement();
        testEntityManager.persist(auditVersement);
    }

    @Test
    public void findById(){
            Optional<Audit> returnedAuditVersement=auditVersementRepository
                    .findById(auditVersement.getId()) ;
        assertTrue(returnedAuditVersement.equals(Optional.of(auditVersement)));
    }

    @Test
    public void delete() {
        boolean beforeDelete=auditVersementRepository.findById(auditVersement.getId()).isPresent();

        auditVersementRepository.delete(auditVersement);

        boolean afterDelete=auditVersementRepository.findById(auditVersement.getId()).isPresent();
        assertTrue(beforeDelete);
        assertFalse(afterDelete);
    }

    @Test
    public void save() {
        Audit returnedAuditVersement=auditVersementRepository.save(auditVersement);
        assertEquals(auditVersement, returnedAuditVersement);
    }

    @Test
    public void findAll() {
        List<Audit> returnedList=auditVersementRepository.findAll();

        List<Audit> myList=new ArrayList<Audit>();
        myList.add(auditVersement);

        assertEquals(myList, returnedList);
    }





}