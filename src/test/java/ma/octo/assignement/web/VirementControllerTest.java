package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.service.VirementService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.mockito.Mockito.verify;

@WebMvcTest(VirementController.class)
class VirementControllerTest {
    /*

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VirementService virementService;

    private Virement virement;

    private List<Virement> myList;

    public Virement createVirement(){
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("user1");
        utilisateur1.setLastname("last1");
        utilisateur1.setFirstname("first1");
        utilisateur1.setGender("Male");

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("user2");
        utilisateur2.setLastname("last2");
        utilisateur2.setFirstname("first2");
        utilisateur2.setGender("Female");

        Compte compte1 = new Compte();
        compte1.setNrCompte("010000A000001000");
        compte1.setRib("RIB1");
        compte1.setSolde(BigDecimal.valueOf(216260.00));
        compte1.setUtilisateur(utilisateur1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("RIB2");
        compte2.setSolde(BigDecimal.valueOf(140000L));
        compte2.setUtilisateur(utilisateur2);

        Virement virement = new Virement();
        virement.setMontantVirement(new BigDecimal(10.00));
        virement.setCompteBeneficiaire(compte2);
        virement.setCompteEmetteur(compte1);
        virement.setDateExecution(new Date("2021-12-03T14:48:04.975+00:00"));
        virement.setMotifVirement("Assignment 2021");
        return virement;
    }
    @BeforeEach
    void setUp() {
        virement=createVirement();
        myList=new ArrayList<Virement>();
        myList.add(virement);
    }

    @Test
    void loadAllVirement() throws Exception {
        Mockito.when(virementService.loadAllVirement()).thenReturn(myList);

        mockMvc.perform(MockMvcRequestBuilders.get("/lister_virements"))
                        .andExpect(MockMvcResultMatchers.status().isOk());
        verify(virementService).loadAllVirement();

    }


    @Test
    void loadAllCompte() {
    }

    @Test
    void loadAllUtilisateur() {
    }

    @Test
    void createTransaction() {
    }*/
}