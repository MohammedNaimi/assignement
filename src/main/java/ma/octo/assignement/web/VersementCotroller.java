package ma.octo.assignement.web;

import ma.octo.assignement.domain.*;
import ma.octo.assignement.domain.util.AccountType;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.IAuditService;
import ma.octo.assignement.service.IVersementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController()
public class VersementCotroller {
    @Autowired
    @Qualifier("AuditService")
    private IAuditService auditService;

    @Autowired
    @Qualifier("VersementService")
    private IVersementService versementService;

    @GetMapping("lister_versements")
    @Transactional
    List<Versement> loadAllVersement() {
        return versementService.loadAllVersement();
    }

    @PostMapping("/executerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public void createTransaction(@RequestBody VersementDto versementDto)  {
        try {
        versementService.doesAccountExist(versementDto.getNrCompteBeneficiaire());

        versementService.isMotifEmpty(versementDto.getMotifVersement());

        versementService.updateSoldeAccount(versementDto.getNrCompteBeneficiaire(), versementDto.getMontantVersement()
                ,AccountType.RECEIVER);

        versementService.createVersement(versementDto);

        auditService.auditVersement(new AuditVersement("Versement vers " + versementDto.getNrCompteBeneficiaire()+ " d'un montant de "
                +versementDto.getMontantVersement(), EventType.VERSEMENT));

        }catch (CompteNonExistantException | TransactionException e){
            System.out.println(e.getMessage());
        }

    }
}
