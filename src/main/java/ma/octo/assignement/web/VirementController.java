package ma.octo.assignement.web;

import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.AccountType;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.dto.VirementDto;

import ma.octo.assignement.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import javax.transaction.Transactional;
import java.util.List;

@RestController()
public class VirementController {


    @Autowired
    @Qualifier("AuditService")
    private IAuditService auditService;

    @Autowired
    @Qualifier("VirementService")
    private IVirementService virementService;

    @GetMapping("lister_virements")
    @Transactional
    List<Virement> loadAllVirement() {
        return virementService.loadAllVirement();
    }

    @GetMapping("lister_comptes")
    @Transactional
    List<Compte> loadAllCompte() {
        return virementService.loadAllCompte();
    }

    @GetMapping("lister_utilisateurs")
    @Transactional
    List<Utilisateur> loadAllUtilisateur() {
        return virementService.loadAllUtilisateur();

    }

    @PostMapping("/executerVirements")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public void createTransaction(@RequestBody VirementDto virementDto) {
        try {
            virementService.doesAccountExist(virementDto.getNrCompteEmetteur());

            virementService.doesAccountExist(virementDto.getNrCompteBeneficiaire());

            virementService.isMontantVirementValid(virementDto.getMontantVirement());

            virementService.isMotifEmpty(virementDto.getMotif());

            virementService.isSoldSufficient(virementDto.getNrCompteEmetteur(), virementDto.getMontantVirement());

            virementService
                    .updateSoldeAccount(virementDto.getNrCompteEmetteur(), virementDto.getMontantVirement(),
                            AccountType.EMITTER);

            virementService
                    .updateSoldeAccount(virementDto.getNrCompteBeneficiaire(), virementDto.getMontantVirement(),
                            AccountType.RECEIVER);

            virementService.createVirement(virementDto);

            auditService.auditVirement(new AuditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                    .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                    .toString(), EventType.VIREMENT) );

        }catch (Exception e){
            System.out.println(e.getMessage());
        }


    }
}
