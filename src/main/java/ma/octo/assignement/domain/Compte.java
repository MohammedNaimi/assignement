package ma.octo.assignement.domain;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "COMPTE")
public class Compte {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 16, unique = true)
  private String nrCompte;

  private String rib;

  @Column(precision = 16, scale = 2)
  private BigDecimal solde;

  @ManyToOne(cascade = CascadeType.PERSIST)
  //@ManyToOne(cascade = CascadeType.DETACH)
  @JoinColumn(name = "utilisateur_id")
  private Utilisateur utilisateur;

  public Compte(Long id, String nrCompte, String rib, BigDecimal solde, Utilisateur utilisateur) {
    this.id = id;
    this.nrCompte = nrCompte;
    this.rib = rib;
    this.solde = solde;
    this.utilisateur = utilisateur;
  }

  public Compte(String nrCompte, String rib, BigDecimal solde, Utilisateur utilisateur) {
    this.nrCompte = nrCompte;
    this.rib = rib;
    this.solde = solde;
    this.utilisateur = utilisateur;
  }

  public Compte() {
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Compte compte = (Compte) o;
    return nrCompte.equals(compte.nrCompte) ;
  }

  @Override
  public int hashCode() {
    return Objects.hash(nrCompte);
  }

  public String getNrCompte() {
    return nrCompte;
  }

  public void setNrCompte(String nrCompte) {
    this.nrCompte = nrCompte;
  }

  public String getRib() {
    return rib;
  }

  public void setRib(String rib) {
    this.rib = rib;
  }

  public BigDecimal getSolde() {
    return solde;
  }

  public void setSolde(BigDecimal solde) {
    this.solde = solde;
  }

  public Utilisateur getUtilisateur() {
    return utilisateur;
  }

  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }
}
