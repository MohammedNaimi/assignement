package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT_VERSEMENT")
public class AuditVersement extends Audit {
  public AuditVersement(String message, EventType eventType) {
    super(message, eventType);
  }

  public AuditVersement() {
  }
}
