package ma.octo.assignement.domain;

import ma.octo.assignement.domain.util.EventType;

import javax.persistence.*;

@Entity
@Table(name = "AUDIT_VIREMENT")
public class AuditVirement extends Audit {
  public AuditVirement() {
  }

  public AuditVirement(String message, EventType eventType) {
    super(message, eventType);
  }
}
