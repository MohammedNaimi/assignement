package ma.octo.assignement.domain.util;

public enum MontantConstraints {
    MONTANT_MAXIMAL(10000),
    MONTANT_MINIMAL(10),
    MONTANT_NULLE(0);

    public final int montant;

    private MontantConstraints(int montant) {
        this.montant = montant;
    }

}
