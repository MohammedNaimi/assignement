package ma.octo.assignement.domain.util;

public enum AccountType {
    EMITTER,
    RECEIVER
}
