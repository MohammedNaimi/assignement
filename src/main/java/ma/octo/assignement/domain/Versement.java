package ma.octo.assignement.domain;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "VERSEMENT")
public class Versement {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(precision = 16, scale = 2, nullable = false)
  private BigDecimal montantVersement;

  @Column
  @Temporal(TemporalType.TIMESTAMP)
  private Date dateExecution;

  @Column
  private String nom_prenom_emetteur;

  @ManyToOne(cascade = CascadeType.PERSIST)
  private Compte compteBeneficiaire;

  @Column(length = 200)
  private String motifVersement;

  public Versement(BigDecimal montantVersement, Date dateExecution, String nom_prenom_emetteur,
                   Compte compteBeneficiaire, String motifVersement) {
    this.montantVersement = montantVersement;
    this.dateExecution = dateExecution;
    this.nom_prenom_emetteur = nom_prenom_emetteur;
    this.compteBeneficiaire = compteBeneficiaire;
    this.motifVersement = motifVersement;
  }

  public Versement() {
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Versement versement = (Versement) o;
    return montantVersement.equals(versement.montantVersement) && dateExecution.equals(versement.dateExecution)
            && nom_prenom_emetteur.equals(versement.nom_prenom_emetteur)
            && compteBeneficiaire.equals(versement.compteBeneficiaire)
            && motifVersement.equals(versement.motifVersement);
  }

  @Override
  public int hashCode() {
    return Objects.hash(montantVersement, dateExecution, nom_prenom_emetteur, compteBeneficiaire, motifVersement);
  }

  public BigDecimal getMontantVersement() {
    return montantVersement;
  }

  public void setMontantVersement(BigDecimal montantVirement) {
    this.montantVersement = montantVirement;
  }

  public Date getDateExecution() {
    return dateExecution;
  }

  public void setDateExecution(Date dateExecution) {
    this.dateExecution = dateExecution;
  }

  public Compte getCompteBeneficiaire() {
    return compteBeneficiaire;
  }

  public void setCompteBeneficiaire(Compte compteBeneficiaire) {
    this.compteBeneficiaire = compteBeneficiaire;
  }

  public String getMotifVersement() {
    return motifVersement;
  }

  public void setMotifVersement(String motifVirement) {
    this.motifVersement = motifVirement;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNom_prenom_emetteur() {
    return nom_prenom_emetteur;
  }

  public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
    this.nom_prenom_emetteur = nom_prenom_emetteur;
  }
}
