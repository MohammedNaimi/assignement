package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.AccountType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.List;

public interface IService {
    public void doesAccountExist(String NrCompte) throws CompteNonExistantException;
    public void isMotifEmpty(String motif) throws TransactionException;
    public List<Compte> loadAllCompte();
    public List<Utilisateur> loadAllUtilisateur();
    public BigDecimal updateSoldeAccount(String nrCompte, BigDecimal montantVirement, AccountType operation);

}
