package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.AccountType;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.List;

public interface IVersementService extends IService {
    //Methodes spécialles au versement
    public void createVersement(VersementDto versementDto);
    public Versement mapToVersement(VersementDto versementDto);
    public List<Versement> loadAllVersement();
}
