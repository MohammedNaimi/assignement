package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;

public interface IAuditService {
    public void auditVirement(Audit audit);
    public void auditVersement(Audit audit);
}
