package ma.octo.assignement.service;

import ma.octo.assignement.domain.Audit;
import ma.octo.assignement.repository.AuditVersementRepository;
import ma.octo.assignement.repository.AuditVirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("AuditService")
@Transactional
public class AuditService implements IAuditService{

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    @Autowired
    private AuditVirementRepository auditVirementRepository;
    @Autowired
    private AuditVersementRepository auditVersementRepository;

    @Override
    public void auditVirement(Audit audit) {

        LOGGER.info("Audit de l'événement {}", audit.getEventType());

        auditVirementRepository.save(audit);
    }


    @Override
    public void auditVersement(Audit audit) {

        LOGGER.info("Audit de l'événement {}", audit.getEventType());

        auditVersementRepository.save(audit);
    }
}
