package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.AccountType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@org.springframework.stereotype.Service
@Transactional
public abstract class Service implements IService {
    @Autowired
    protected CompteRepository compteRepository;
    @Autowired
    protected UtilisateurRepository utilisateurRepository;

    @Override
    public void doesAccountExist(String NrCompte) throws CompteNonExistantException{
        Compte compte = compteRepository.findByNrCompte(NrCompte);
        if (compte == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
    }

    @Override
    public void isMotifEmpty(String motif) throws TransactionException {
        if (motif.length() == 0) {
            System.out.println("Motif vide");
            throw new TransactionException("Motif vide");
        }
    }

    @Override
    public List<Compte>  loadAllCompte() {
        List<Compte> all = compteRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    @Override
    public List<Utilisateur> loadAllUtilisateur() {
        List<Utilisateur> all = utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    public BigDecimal updateSoldeAccount(String nrCompte, BigDecimal montantVirement, AccountType operation){
        Compte compte = compteRepository.findByNrCompte(nrCompte);
        if(operation==AccountType.EMITTER){
            compte.setSolde(new BigDecimal(compte.getSolde().intValue()-montantVirement.intValue()) );
            compteRepository.save(compte);

        }else if(operation==AccountType.RECEIVER){
            compte.setSolde( compte.getSolde().add(montantVirement));
            compteRepository.save(compte);
        }
        return compte.getSolde();
    }
}
