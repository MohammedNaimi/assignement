package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.util.List;

@org.springframework.stereotype.Service("VersementService")
@Transactional
public class VersementService extends Service implements IVersementService{
    @Autowired
    private VersementRepository versementRepository;

    public List<Versement> loadAllVersement(){
        List<Versement> all = versementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    public Versement mapToVersement(VersementDto versementDto) {
        Versement versement = new Versement();
        versement.setDateExecution(versementDto.getDate());

        versement.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());

        Compte compteBeneficiaire = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());
        versement.setCompteBeneficiaire(compteBeneficiaire);


        versement.setMotifVersement(versementDto.getMotifVersement());
        versement.setMontantVersement(versementDto.getMontantVersement());
        return versement;
    }

    public void createVersement(VersementDto versementDto){
        Versement versement= mapToVersement(versementDto);
        versementRepository.save(versement);

    }
}
