package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.MontantConstraints;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.web.VirementController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@org.springframework.stereotype.Service("VirementService")
@Transactional
public class VirementService extends Service implements IVirementService{

    @Autowired
    private VirementRepository virementRepository;

    public List<Virement> loadAllVirement() {
        List<Virement> all = virementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    public void isMontantVirementValid(BigDecimal montantVirement) throws TransactionException{
        if (montantVirement==null) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");

        } else if (montantVirement.intValue() == MontantConstraints.MONTANT_NULLE.montant) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");

        } else if (montantVirement.intValue() < MontantConstraints.MONTANT_MINIMAL.montant) {
            System.out.println("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");

        } else if (montantVirement.intValue() > MontantConstraints.MONTANT_MAXIMAL.montant) {
            System.out.println("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");

        }
    }

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);
    public void isSoldSufficient(String nrCompte, BigDecimal montantVirement)
            throws SoldeDisponibleInsuffisantException {
        Compte compte = compteRepository.findByNrCompte(nrCompte);

        if (compte.getSolde().intValue() - montantVirement.intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }


    }

    public Virement mapToVirement(VirementDto virementDto) {
        Virement virement = new Virement();
        virement.setDateExecution(virementDto.getDate());

        Compte compteBeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());
        virement.setCompteBeneficiaire(compteBeneficiaire);

        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMotifVirement(virementDto.getMotif());
        virement.setMontantVirement(virementDto.getMontantVirement());
        return virement;
    }

    public void createVirement(VirementDto virementDto){
        Virement virement= mapToVirement(virementDto);
        virementRepository.save(virement);
    }

}
