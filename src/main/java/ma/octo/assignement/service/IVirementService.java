package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.AccountType;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.math.BigDecimal;
import java.util.List;

public interface IVirementService extends IService{

    public List<Virement> loadAllVirement();
    public void isMontantVirementValid(BigDecimal montantVirement) throws TransactionException;
    public void isSoldSufficient(String nrCompte, BigDecimal montantVirement)
            throws SoldeDisponibleInsuffisantException;

    public Virement mapToVirement(VirementDto virementDto);
    public void createVirement(VirementDto virementDto);
}
