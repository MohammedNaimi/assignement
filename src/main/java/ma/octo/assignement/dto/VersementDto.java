package ma.octo.assignement.dto;

import ma.octo.assignement.domain.Compte;


import java.math.BigDecimal;
import java.util.Date;

public class VersementDto {
    private BigDecimal montantVersement;
    private Date date;
    private String nom_prenom_emetteur;
    private String nrCompteBeneficiaire;
    private String motifVersement;

    public VersementDto(BigDecimal montantVersement, Date date, String nom_prenom_emetteur, String nrCompteBeneficiaire
            , String motifVersement) {
        this.montantVersement = montantVersement;
        this.date = date;
        this.nom_prenom_emetteur = nom_prenom_emetteur;
        this.nrCompteBeneficiaire = nrCompteBeneficiaire;
        this.motifVersement = motifVersement;
    }

    public VersementDto() {
    }

    public void setMontantVersement(BigDecimal montantVirement) {
        this.montantVersement = montantVirement;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
        this.nom_prenom_emetteur = nom_prenom_emetteur;
    }

    public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
        this.nrCompteBeneficiaire = nrCompteBeneficiaire;
    }

    public void setMotifVersement(String motifVersement) {
        this.motifVersement = motifVersement;
    }

    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public Date getDate() {
        return date;
    }

    public String getNom_prenom_emetteur() {
        return nom_prenom_emetteur;
    }

    public String getNrCompteBeneficiaire() {
        return nrCompteBeneficiaire;
    }

    public String getMotifVersement() {
        return motifVersement;
    }


}
